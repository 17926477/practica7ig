package mx.unitec.moviles.practica7.widget

import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.ItemTouchHelper
import mx.unitec.moviles.practica7.model.Contact

interface NoticeDialogListener {
    fun onDialogPositiveClick(dialog: DialogFragment, contact: Contact)
    fun onDialogNegativeClick(dialog: DialogFragment)
    fun ItemTouchHelper(swipeHandler: Any) : ItemTouchHelper
}